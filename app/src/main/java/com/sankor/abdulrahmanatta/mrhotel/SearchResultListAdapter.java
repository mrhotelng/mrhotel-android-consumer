package com.sankor.abdulrahmanatta.mrhotel;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import pojo.SearchResultPojo;

/**
 * Created by abdulrahmanatta on 15-06-27.
 */
public class SearchResultListAdapter extends ArrayAdapter<SearchResultPojo> {



    private Typeface fontRoboLight;
    private Typeface fontRoboBold ;
    private Context context;

    private List<SearchResultPojo> objects; //so it doesnt have to reload into memory all the time
    private List<String> imageUrls = new ArrayList<>();
    public SearchResultListAdapter(Context context, int resource, List<SearchResultPojo> objects) {
        super(context, resource, objects);
        fontRoboLight =  Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        fontRoboBold =  Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf");
        this.context = context;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SearchResultPojo object = objects.get(position);
        this.context = parent.getContext();
        LayoutInflater  inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.search_result_list_item,null); //find the resource xml for the custom item

        ImageView imageView = (ImageView) view.findViewById(R.id.listpictiure); //find and set the image here
        imageView.setImageResource(R.drawable.hi_res_icon_1);

        //find and set the Text here
        TextView title = (TextView) view.findViewById(R.id.search_result_item_hotel_name);
        title.setTypeface(fontRoboLight);
        title.setText(object.getHotelRoom().getName());

        //set the price here
        TextView priceView = (TextView) view.findViewById(R.id.hotel_price);
        priceView.setTypeface(fontRoboBold);
        double price = object.getCheckinDayPrice()/1000;
        String priceToShow = String.valueOf(price)+"K";
        priceView.setText(priceToShow);

        //set the address of the hotel
        TextView addressView = (TextView) view.findViewById(R.id.hotel_address);
        addressView.setTypeface(fontRoboLight);
        String addressString = object.getHotel().getAddress();
        addressView.setText(addressString);

        //set the name of the room
        TextView roomNameView = (TextView) view.findViewById(R.id.room_name);
        roomNameView.setTypeface(fontRoboBold);
        roomNameView.setText(object.getHotel().getName());


        //calculate the percentage off and parse it here
        //get the original price
        Integer originalPrice  = object.getHotelRoom().getOriginalPrice();
        Integer priceOnNight = object.getCheckinDayPrice();
        Integer percentageDiscount = ((originalPrice-priceOnNight)/originalPrice)*100;
        TextView percentageDiscountView = (TextView) view.findViewById(R.id.percentage_off);
        percentageDiscountView.setTypeface(fontRoboLight);
        percentageDiscountView.setText("@\n" + Integer.toString(percentageDiscount) + "% Off");
        //percentageDiscountView.setPaintFlags(percentageDiscountView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);



        //get the star rating here
        Integer starRating = object.getHotel().getStarRating();
        boolean singular=false;
        if(starRating==1)  singular = true;
        String toShow = Integer.toString(starRating)+" Star";
        if(!singular){
            toShow+="s";
        }
        TextView ratingBox = (TextView) view.findViewById(R.id.star_rating);
        ratingBox.setTypeface(fontRoboLight);
        ratingBox.setText(toShow);
        //set the amneities
        TextView ammenities = (TextView) view.findViewById(R.id.amnenities);
        ammenities.setTypeface(fontRoboLight);
        String finalString = "Amenities include";

        if(object.getHotel().isFreeWifi()){
            finalString+=" Wifi";
        }
        if(object.getHotel().isSwimmingPool()){
            finalString+=",Swimming pool";
        }
        if(object.getHotel().isAirportShuttle()){
            finalString+=",Airport Shuttle";
        }
        ammenities.setText(finalString);
      /*  if(object.getPhoto() != null){
           ImageView imager = (ImageView) view.findViewById(R.id.listpictiure);
            imager.setScaleType(ImageView.ScaleType.FIT_XY);
            imager.setImageBitmap(object.getPhoto());
        }*/

            DataToPass dataToPass = new DataToPass();
            dataToPass.searchResultPojo= object;
            dataToPass.theView = view;
            ImageLoader loader = new ImageLoader();
            loader.execute(dataToPass);

        return  view;
    }
    class DataToPass{
        public SearchResultPojo searchResultPojo;
        public View theView;
        public Bitmap bitmap;
    }

    private class ImageLoader extends AsyncTask<DataToPass,Void,DataToPass>{


        @Override
        protected DataToPass doInBackground(DataToPass... dataToPasses) {
            DataToPass container = dataToPasses[0];
            SearchResultPojo searchResultPojo = container.searchResultPojo;

            try {
                String imageObjString = searchResultPojo.getImage();
                JsonParser jsonparser = new JsonParser();
                JsonObject o = (JsonObject)jsonparser.parse(imageObjString);
                String imageUrl =  o.get("url").getAsString();
                InputStream in = (InputStream) new URL(imageUrl).getContent();
                Bitmap bitmap = BitmapFactory.decodeStream(in);
                //searchResultPojo.setPhoto(bitmap);
                in.close();
                container.bitmap = bitmap;
                return container;
            }catch (Exception e){

                e.printStackTrace();

            }

            return null;
        }

        @Override
        protected void onPostExecute(DataToPass dataToPass) {
            ImageView imageV = (ImageView) dataToPass.theView.findViewById(R.id.listpictiure);
            imageV.setImageBitmap(dataToPass.bitmap);
            imageV.setScaleType(ImageView.ScaleType.FIT_XY);
           // dataToPass.searchResultPojo.setPhoto(dataToPass.bitmap);

        }
    }
}
