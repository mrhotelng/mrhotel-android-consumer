package com.sankor.abdulrahmanatta.mrhotel;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import constants.AppConstants;
import pojo.HotelPlacesPojo;
import pojo.HotelSearchPojo;
import pojo.SearchHotelResultDto;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import utility.GpsUtility;
import utility.SearchApi;


public class MainSearch extends Activity {
    private static final String TAG = "MainSearch";
    private double lat;
    private double lon;
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd 00:00:00");
    private List<String> hotelPlaces = new ArrayList<>();
    private final String[] DURATION = new String[]{"1 Night", "2 Nights", "3 Nights", "4 Nights", "5 Nights", "6 Nights"};
    private final String[] NUMBEROFROOMS = new String[]{"1 Room","2 Rooms","3 Rooms","4 Rooms","5 ROoms","6 Rooms","7 Rooms"};
    private HashMap<String,Calendar> dateMapper = new HashMap<>();
    private String placeSearched = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_search);


        setDateOptions();
        setDurationOptions();
        getAllHotelPlaces();
        setNumberOfRoomsOptions();

        GpsUtility gps = new GpsUtility(this);
        if (gps.canGetLocation()) {
            lat = gps.getLatitude(); // returns latitude
            lon = gps.getLongitude(); // returns longitude
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //method to get the next 7 days
    private String[] days = new String[6];

    private void setDateOptions() {
        //get todays date and map it as today
        Calendar cal = Calendar.getInstance();
        for (int x = 0; x < days.length; x++) {
            //create a new Calendar class to clone the object
           Calendar single = Calendar.getInstance();
           single.setTime(cal.getTime());

            if (x == 0) {
                days[x] = "Today";
                dateMapper.put(days[x],single);
                cal.add(cal.DATE, 1);


            } else if (x == 1) {


                days[x] = "Tomorrow";
                dateMapper.put(days[x], single);
                cal.add(cal.DATE, 1);

            } else {
                String toShow = String.valueOf(cal.get(cal.DAY_OF_MONTH) + " of " + AppConstants.getMonth((cal.get(cal.MONTH) + 1)) + "," + String.valueOf(cal.get(cal.YEAR)));
                days[x] = toShow;
                dateMapper.put(days[x],single);
                cal.add(cal.DATE, 1);
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, days);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner sItems = (Spinner) findViewById(R.id.datePick);
        sItems.setAdapter(adapter);

    }

    private void setDurationOptions() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, DURATION);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner sItems = (Spinner) findViewById(R.id.numberOfNights);
        sItems.setAdapter(adapter);
    }


    public void getAllHotelPlaces() {
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(getString(R.string.server))
                .setRequestInterceptor(requestInterceptor)
                .build();

        SearchApi api = adapter.create(SearchApi.class);
        api.getAllHotelPlaces(new Callback<List<HotelPlacesPojo>>() {
            @Override
            public void success(List<HotelPlacesPojo> hotelPlacesPojos, Response response) {

                for (HotelPlacesPojo place : hotelPlacesPojos) {
                    hotelPlaces.add(place.getCity() + "," + place.getState());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, hotelPlaces);
                AutoCompleteTextView textView = (AutoCompleteTextView)
                        findViewById(R.id.multiAutoCompleteTextView);
                textView.setAdapter(adapter);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), getString(R.string.server_error), Toast.LENGTH_LONG).show();
                Log.e(TAG, error.toString());
            }
        });


    }


    private void setNumberOfRoomsOptions(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, NUMBEROFROOMS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner sItems = (Spinner) findViewById(R.id.numberOfRooms);
        sItems.setAdapter(adapter);
    }






    public void searchDeals(View view) {
        //get the location data and send it up
        AutoCompleteTextView location = (AutoCompleteTextView) findViewById(R.id.multiAutoCompleteTextView);
        String locationText = location.getEditableText().toString();
        placeSearched = locationText;

        //check if the user put any location there
        if(locationText.isEmpty() || locationText.contains("D/MainSearch")){
            Toast.makeText(getApplicationContext(), "please enter a location", Toast.LENGTH_LONG).show();
            return;
        }

        //get the calendar day and parse the data
        Spinner date = (Spinner) findViewById(R.id.datePick);
        Calendar dateText = dateMapper.get(date.getSelectedItem().toString());
        String dateToGo = sdf.format(dateText.getTime());



        //get the duration here
        Spinner duration = (Spinner) findViewById(R.id.numberOfNights);
        String durationText = duration.getSelectedItem().toString();



        //get the number of rooms here
        Spinner numberOfRooms = (Spinner) findViewById(R.id.numberOfRooms);
        String numberOfRoomsText = numberOfRooms.getSelectedItem().toString();


        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(getString(R.string.server))
                //.setRequestInterceptor(requestInterceptor)
                .build();

        SearchApi api = adapter.create(SearchApi.class);
        //create the POJO to send to the server
        Log.d(TAG,dateToGo);
        Log.d(TAG,locationText);
        Log.d(TAG,durationText);
        Log.d(TAG,numberOfRoomsText);

        final HotelSearchPojo searchPojo = new HotelSearchPojo(dateToGo, locationText, durationText, numberOfRoomsText);

        api.searchHotels(searchPojo, new Callback<SearchHotelResultDto>() {


            /**
             * Successful HTTP response.
             *
             * @param searchResultPojos
             * @param response
             */
            @Override
            public void success(SearchHotelResultDto searchResultPojos, Response response) {

                Log.i(TAG, "Response success");
                Intent searchResult = new Intent(MainSearch.this, SearchResult.class);
                searchResult.putExtra("searchResult", searchResultPojos);
                searchResult.putExtra("placeSearched",placeSearched);

                startActivity(searchResult);

            }

            /**
             * Unsuccessful HTTP response due to network failure, non-2XX status code, or unexpected
             * exception.
             *
             * @param error
             */
            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), getString(R.string.server_error), Toast.LENGTH_LONG).show();
                Log.e(TAG, error.toString());

            }
        });

    }

    RequestInterceptor requestInterceptor = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade request) {
            request.addHeader("Authorization", "dawnhjnsjd2nas5dsmfpmm");  //token should be stored here
        }
    };



}
