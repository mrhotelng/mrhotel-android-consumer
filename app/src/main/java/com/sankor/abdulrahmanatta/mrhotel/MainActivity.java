package com.sankor.abdulrahmanatta.mrhotel;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //remove the action bar at on the homepage
        setContentView(R.layout.activity_main);
        //login button gotten here
        final Button loginButton = (Button) findViewById(R.id.button);
        //loginButton.getBackground().setAlpha(128);


        //register button gotten here
        final Button registerButton = (Button) findViewById(R.id.button2);
        // registerButton.getBackground().setAlpha(128);

        //add event handler to simulate enter button clicking the login button here
        EditText edit_txt = (EditText) findViewById(R.id.editText2);
        edit_txt.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loginButton.callOnClick();
                    return true;
                }
                return false;
            }
        });
        //end of event handler


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void loginAction(View view) {
        Signin signin = new Signin();
        signin.doInBackground("usernamehere", "password here");
        Intent searchPage = new Intent(this, MainSearch.class);
        startActivity(searchPage);


    }

    private class Signin extends AsyncTask<String, String, String> {
        private Context context = getApplicationContext();

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... strings) {
            String username = strings[0];
            String password = strings[1];
            Toast.makeText(context, username, Toast.LENGTH_LONG).show();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

        }
    }


}
