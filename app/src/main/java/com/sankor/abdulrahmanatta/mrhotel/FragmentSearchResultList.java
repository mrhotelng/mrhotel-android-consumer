package com.sankor.abdulrahmanatta.mrhotel;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import pojo.SearchHotelResultDto;
import pojo.SearchResultPojo;

/**
 * Created by abdulrahmanatta on 15-06-27.
 */
public class FragmentSearchResultList extends Fragment implements OnItemClickListener {

    private View mainView ;
    private ListView theview;
    private SearchHotelResultDto result;
    private List<SearchResultPojo> hotelSearch;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_list_search_result,container,false);
            this.mainView = view;
            return view;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SearchResultPojo sampleModel = new SearchResultPojo();
        result = ((SearchResult) getActivity()).getResult();
        hotelSearch = result.getData();
        SearchResultListAdapter mainList = new SearchResultListAdapter(getActivity(),R.layout.search_result_list_item,hotelSearch);
        theview = (ListView) mainView.findViewById(R.id.list);
        theview.setAdapter(mainList);

        theview.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //get the object here and launch the new intent
                SearchResultPojo searchResultPojo = hotelSearch.get(position);
                Intent intent = new Intent(getActivity(), HotelDetails.class);
                intent.putExtra("hotelName",searchResultPojo.getHotel().getName());
                intent.putExtra("hotelDescription",searchResultPojo.getHotel().getDescription());
                intent.putExtra("contactEmail",searchResultPojo.getHotel().getContactEmail());
                intent.putExtra("starRating",searchResultPojo.getHotel().getStarRating());
                intent.putExtra("roomName",searchResultPojo.getHotelRoom().getName());
                intent.putExtra("roomPrice",searchResultPojo.getPrice());
                intent.putExtra("roomPicture",searchResultPojo.getImage());
                startActivity(intent);
                Toast.makeText(getActivity(), Integer.toString(position), Toast.LENGTH_LONG).show();

            }
        });

    }




    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {



    }
}
