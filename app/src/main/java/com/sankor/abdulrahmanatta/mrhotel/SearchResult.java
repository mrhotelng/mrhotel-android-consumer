package com.sankor.abdulrahmanatta.mrhotel;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

import pojo.SearchHotelResultDto;
import utility.GpsUtility;


public class SearchResult extends ActionBarActivity {

    private GoogleMap map;

    private SearchHotelResultDto result;

    public SearchHotelResultDto getResult() {
        return result;
    }

    private double lat;


    private  double lon;

    public void setResult(SearchHotelResultDto result) {
        this.result = result;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        result = (SearchHotelResultDto) getIntent().getSerializableExtra("searchResult");
        setTitle("Results for "+getIntent().getStringExtra("placeSearched")); //set the title to the place being searched
        if(result.getData().isEmpty()){
            Toast.makeText(getApplicationContext(), getString(R.string.empty_result), Toast.LENGTH_LONG).show();

            return;
        }
        lat  = result.getData().get(0).getLat();
        lon  = result.getData().get(0).getLon();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search_result, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    protected void onResume() {
        super.onResume();
        GpsUtility gps = new GpsUtility(this);
        if (gps.canGetLocation()){
            setMapUp(lat,lon);
        }else{
            setMapUp(lat,lon); //set the default location to ikeja Lagos Nigeria
        }

    }

    public void setMapUp(double lat, double lon) {
        SupportMapFragment mapFragmentClass= new SupportMapFragment ();
        getSupportFragmentManager().beginTransaction().add(R.id.map, mapFragmentClass).commit();
        getSupportFragmentManager().executePendingTransactions();
        map = mapFragmentClass.getMap();
        map.setMyLocationEnabled(true);
        CameraUpdate center =
                CameraUpdateFactory.newLatLng(new LatLng(lat,
                        lon));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(13);
        map.moveCamera(center);
        map.animateCamera(zoom);
        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(new LatLng(lat, lon));
        circleOptions.radius(1350);
        circleOptions.fillColor(R.color.map_filling);
        Circle circle = map.addCircle(circleOptions);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //SaveState saveState = new SaveState();
        //saveState.searchHotelResultDto = result;
        //outState.putSerializable("searchResult", saveState);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
       // SaveState xx = (SaveState)savedInstanceState.getSerializable("searchResult");
        //result = xx.searchHotelResultDto;

    }

    public double getLat() {
        return lat;
    }


    public double getLon() {
        return lon;
    }

    public class SaveState implements Serializable{
        public SearchHotelResultDto searchHotelResultDto;
    }
}
