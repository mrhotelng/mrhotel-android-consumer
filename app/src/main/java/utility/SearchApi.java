package utility;

import java.util.List;

import pojo.HotelPlacesPojo;
import pojo.HotelSearchPojo;
import pojo.SearchHotelResultDto;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;

/**
 * Created by abdulrahmanatta on 15-07-01.
 */
public interface SearchApi {

    @POST("/api/HotelHotelavailabilities/searchAvailability")
    void searchHotels(@Body HotelSearchPojo searchObject, Callback<SearchHotelResultDto> response);

    @GET("/api/HotelPlaces")
    void getAllHotelPlaces(Callback<List<HotelPlacesPojo>> response);


}
