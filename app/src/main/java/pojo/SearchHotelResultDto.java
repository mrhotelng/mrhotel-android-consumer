package pojo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by abdulrahmanatta on 15-07-02.
 */
public class SearchHotelResultDto  implements Serializable{
    private List<SearchResultPojo> data;

    public List<SearchResultPojo> getData() {
        return data;
    }

    public void setData(List<SearchResultPojo> data) {
        this.data = data;
    }
}
