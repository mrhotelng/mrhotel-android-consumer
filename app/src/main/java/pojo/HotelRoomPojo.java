package pojo;

import java.io.Serializable;

/**
 * Created by abdulrahmanatta on 15-07-05.
 */
public class HotelRoomPojo implements Serializable{
    private Integer id;
    private String name;
    private Integer originalPrice;
    private Integer hotelId;
    private String create;
    private String updates;
    //private ImagePojo image;
    private String image;
    private boolean compBreakfast;
    private boolean miniFridge;
    private Integer numBeds;
    private boolean roomService;
    private boolean tv;
    private boolean isActive;
    private Integer inventoryDaysleft1;
    private Integer inventoryDaysleft2;
    private Integer inventoryDaysleft3;
    private Integer inventoryDaysleft4;
    private Integer inventoryDaysleft5;
    private Integer inventoryDaysleft6;
    private Integer inventoryDaysleft7;
    private Integer discountDaysleft1;
    private Integer discountDaysleft2;
    private Integer discountDaysleft3;
    private Integer discountDaysleft4;
    private Integer discountDaysleft5;
    private Integer discountDaysleft6;
    private Integer discountDaysleft7;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(Integer originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Integer getHotelId() {
        return hotelId;
    }

    public void setHotelId(Integer hotelId) {
        this.hotelId = hotelId;
    }

    public String getCreate() {
        return create;
    }

    public void setCreate(String create) {
        this.create = create;
    }

    public String getUpdates() {
        return updates;
    }

    public void setUpdates(String updates) {
        this.updates = updates;
    }

   /* public ImagePojo getImage() {
        return image;
    }

    public void setImage(ImagePojo image) {
        this.image = image;
    }*/

    public boolean isCompBreakfast() {
        return compBreakfast;
    }

    public void setCompBreakfast(boolean compBreakfast) {
        this.compBreakfast = compBreakfast;
    }

    public boolean isMiniFridge() {
        return miniFridge;
    }

    public void setMiniFridge(boolean miniFridge) {
        this.miniFridge = miniFridge;
    }

    public Integer getNumBeds() {
        return numBeds;
    }

    public void setNumBeds(Integer numBeds) {
        this.numBeds = numBeds;
    }

    public boolean isRoomService() {
        return roomService;
    }

    public void setRoomService(boolean roomService) {
        this.roomService = roomService;
    }

    public boolean isTv() {
        return tv;
    }

    public void setTv(boolean tv) {
        this.tv = tv;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Integer getInventoryDaysleft1() {
        return inventoryDaysleft1;
    }

    public void setInventoryDaysleft1(Integer inventoryDaysleft1) {
        this.inventoryDaysleft1 = inventoryDaysleft1;
    }

    public Integer getInventoryDaysleft2() {
        return inventoryDaysleft2;
    }

    public void setInventoryDaysleft2(Integer inventoryDaysleft2) {
        this.inventoryDaysleft2 = inventoryDaysleft2;
    }

    public Integer getInventoryDaysleft3() {
        return inventoryDaysleft3;
    }

    public void setInventoryDaysleft3(Integer inventoryDaysleft3) {
        this.inventoryDaysleft3 = inventoryDaysleft3;
    }

    public Integer getInventoryDaysleft4() {
        return inventoryDaysleft4;
    }

    public void setInventoryDaysleft4(Integer inventoryDaysleft4) {
        this.inventoryDaysleft4 = inventoryDaysleft4;
    }

    public Integer getInventoryDaysleft5() {
        return inventoryDaysleft5;
    }

    public void setInventoryDaysleft5(Integer inventoryDaysleft5) {
        this.inventoryDaysleft5 = inventoryDaysleft5;
    }

    public Integer getInventoryDaysleft6() {
        return inventoryDaysleft6;
    }

    public void setInventoryDaysleft6(Integer inventoryDaysleft6) {
        this.inventoryDaysleft6 = inventoryDaysleft6;
    }

    public Integer getInventoryDaysleft7() {
        return inventoryDaysleft7;
    }

    public void setInventoryDaysleft7(Integer inventoryDaysleft7) {
        this.inventoryDaysleft7 = inventoryDaysleft7;
    }

    public Integer getDiscountDaysleft1() {
        return discountDaysleft1;
    }

    public void setDiscountDaysleft1(Integer discountDaysleft1) {
        this.discountDaysleft1 = discountDaysleft1;
    }

    public Integer getDiscountDaysleft2() {
        return discountDaysleft2;
    }

    public void setDiscountDaysleft2(Integer discountDaysleft2) {
        this.discountDaysleft2 = discountDaysleft2;
    }

    public Integer getDiscountDaysleft3() {
        return discountDaysleft3;
    }

    public void setDiscountDaysleft3(Integer discountDaysleft3) {
        this.discountDaysleft3 = discountDaysleft3;
    }

    public Integer getDiscountDaysleft4() {
        return discountDaysleft4;
    }

    public void setDiscountDaysleft4(Integer discountDaysleft4) {
        this.discountDaysleft4 = discountDaysleft4;
    }

    public Integer getDiscountDaysleft5() {
        return discountDaysleft5;
    }

    public void setDiscountDaysleft5(Integer discountDaysleft5) {
        this.discountDaysleft5 = discountDaysleft5;
    }

    public Integer getDiscountDaysleft6() {
        return discountDaysleft6;
    }

    public void setDiscountDaysleft6(Integer discountDaysleft6) {
        this.discountDaysleft6 = discountDaysleft6;
    }

    public Integer getDiscountDaysleft7() {
        return discountDaysleft7;
    }

    public void setDiscountDaysleft7(Integer discountDaysleft7) {
        this.discountDaysleft7 = discountDaysleft7;
    }
}
