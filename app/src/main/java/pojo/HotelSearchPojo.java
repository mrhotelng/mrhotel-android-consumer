package pojo;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;

/**
 * Created by abdulrahmanatta on 15-07-01.
 */
public class HotelSearchPojo extends OutputStream implements Serializable {

    private String check_in;
    private String location;
    private String duration;
    private String number_of_rooms;


    public HotelSearchPojo(String check_in, String location, String duration, String number_of_rooms) {
        this.check_in = check_in;
        this.location = location;
        this.duration = duration;
        this.number_of_rooms = number_of_rooms;
    }



    public String getNumber_of_rooms() {
        return number_of_rooms;
    }

    public void setNumber_of_rooms(String number_of_rooms) {
        this.number_of_rooms = number_of_rooms;
    }

    public String getCheck_in() {
        return check_in;
    }

    public void setCheck_in(String check_in) {
        this.check_in = check_in;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }


    @Override
    public void write(int i) throws IOException {

    }
}
