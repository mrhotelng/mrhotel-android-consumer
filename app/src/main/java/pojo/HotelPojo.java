package pojo;

import java.io.Serializable;

/**
 * Created by abdulrahmanatta on 15-07-05.
 */
public class HotelPojo implements Serializable {

    private Integer id;
    private String name;
    private String description;
    private Integer placeId;
    private String hotelType;
    private String phoneNumber;
    private String create;
    private String updated;
    private String address;
    private double lat;
    private double lon;
    private boolean fitnessCenter;
    private boolean freeWifi;
    private boolean restaurant;
    private boolean secureParking;
    private boolean swimmingPool;
    private boolean cash;
    private boolean mastercard;
    private boolean visa;
    private boolean airportShuttle;
    private boolean spa;
    private boolean verve;
    //private List<ImagePojo>  image = new ArrayList<>();
    private String image;
    private String uuid;
    private String contactEmail;
    private String contactPhone;
    private String contactName;
    private String town;
    private boolean isSecret;
    private Integer starRating;
    private boolean wifiInternet;
    private boolean businessCenter;

/*    public List<ImagePojo> getImage() {
        return image;
    }

    public void setImage(List<ImagePojo> image) {
        this.image = image;
    }*/

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPlaceId() {
        return placeId;
    }

    public void setPlaceId(Integer placeId) {
        this.placeId = placeId;
    }

    public String getHotelType() {
        return hotelType;
    }

    public void setHotelType(String hotelType) {
        this.hotelType = hotelType;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCreate() {
        return create;
    }

    public void setCreate(String create) {
        this.create = create;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public boolean isFitnessCenter() {
        return fitnessCenter;
    }

    public void setFitnessCenter(boolean fitnessCenter) {
        this.fitnessCenter = fitnessCenter;
    }

    public boolean isFreeWifi() {
        return freeWifi;
    }

    public void setFreeWifi(boolean freeWifi) {
        this.freeWifi = freeWifi;
    }

    public boolean isRestaurant() {
        return restaurant;
    }

    public void setRestaurant(boolean restaurant) {
        this.restaurant = restaurant;
    }

    public boolean isSecureParking() {
        return secureParking;
    }

    public void setSecureParking(boolean secureParking) {
        this.secureParking = secureParking;
    }

    public boolean isSwimmingPool() {
        return swimmingPool;
    }

    public void setSwimmingPool(boolean swimmingPool) {
        this.swimmingPool = swimmingPool;
    }

    public boolean isCash() {
        return cash;
    }

    public void setCash(boolean cash) {
        this.cash = cash;
    }

    public boolean isMastercard() {
        return mastercard;
    }

    public void setMastercard(boolean mastercard) {
        this.mastercard = mastercard;
    }

    public boolean isVisa() {
        return visa;
    }

    public void setVisa(boolean visa) {
        this.visa = visa;
    }

    public boolean isAirportShuttle() {
        return airportShuttle;
    }

    public void setAirportShuttle(boolean airportShuttle) {
        this.airportShuttle = airportShuttle;
    }

    public boolean isSpa() {
        return spa;
    }

    public void setSpa(boolean spa) {
        this.spa = spa;
    }

    public boolean isVerve() {
        return verve;
    }

    public void setVerve(boolean verve) {
        this.verve = verve;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public boolean isSecret() {
        return isSecret;
    }

    public void setIsSecret(boolean isSecret) {
        this.isSecret = isSecret;
    }

    public Integer getStarRating() {
        return starRating;
    }

    public void setStarRating(Integer starRating) {
        this.starRating = starRating;
    }

    public boolean isWifiInternet() {
        return wifiInternet;
    }

    public void setWifiInternet(boolean wifiInternet) {
        this.wifiInternet = wifiInternet;
    }

    public boolean isBusinessCenter() {
        return businessCenter;
    }

    public void setBusinessCenter(boolean businessCenter) {
        this.businessCenter = businessCenter;
    }
}
