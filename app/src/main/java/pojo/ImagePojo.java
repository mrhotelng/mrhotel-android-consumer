package pojo;

import java.io.Serializable;

/**
 * Created by abdulrahmanatta on 15-07-05.
 */
public class ImagePojo implements Serializable {
    private Integer size;
    private Integer height;
    private boolean isWritable;
    private Integer width;
    private String filename;
    private String mimetype;
    private String url;

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public boolean isWritable() {
        return isWritable;
    }

    public void setIsWritable(boolean isWritable) {
        this.isWritable = isWritable;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
