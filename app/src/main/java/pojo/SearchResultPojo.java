package pojo;

import java.io.Serializable;

/**
 * Created by abdulrahmanatta on 15-06-24.
 */
public class SearchResultPojo implements Serializable{

    private String totalAvailable;
    private String created;
    private String updated;
    private Integer totalSold;
    private Integer checkinDayPrice;
    private Integer roomId;
    private String overrideDiscount;
    private String overrideAvailable;
    private String availableDate;
    private Integer hotelId;
    private Integer hotelHotel;
    private HotelPojo hotel;
    private HotelRoomPojo hotelRoom;
    private String image;
    //private Bitmap photo;
    private double lat;
    private double lon;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

   // public Bitmap getPhoto() {
  //      return photo;
   // }

    //public void setPhoto(Bitmap photo) {
      //  this.photo = photo;
    //}

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTotalAvailable() {
        return totalAvailable;
    }

    public void setTotalAvailable(String totalAvailable) {
        this.totalAvailable = totalAvailable;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public Integer getTotalSold() {
        return totalSold;
    }

    public void setTotalSold(Integer totalSold) {
        this.totalSold = totalSold;
    }

    public Integer getCheckinDayPrice() {
        return checkinDayPrice;
    }

    public void setCheckinDayPrice(Integer checkinDayPrice) {
        this.checkinDayPrice = checkinDayPrice;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public String getOverrideDiscount() {
        return overrideDiscount;
    }

    public void setOverrideDiscount(String overrideDiscount) {
        this.overrideDiscount = overrideDiscount;
    }

    public String getOverrideAvailable() {
        return overrideAvailable;
    }

    public void setOverrideAvailable(String overrideAvailable) {
        this.overrideAvailable = overrideAvailable;
    }

    public String getAvailableDate() {
        return availableDate;
    }

    public void setAvailableDate(String availableDate) {
        this.availableDate = availableDate;
    }

    public Integer getHotelId() {
        return hotelId;
    }

    public void setHotelId(Integer hotelId) {
        this.hotelId = hotelId;
    }

    public Integer getHotelHotel() {
        return hotelHotel;
    }

    public void setHotelHotel(Integer hotelHotel) {
        this.hotelHotel = hotelHotel;
    }

    public HotelPojo getHotel() {
        return hotel;
    }

    public void setHotel(HotelPojo hotel) {
        this.hotel = hotel;
    }

    public HotelRoomPojo getHotelRoom() {
        return hotelRoom;
    }

    public void setHotelRoom(HotelRoomPojo hotelRoom) {
        this.hotelRoom = hotelRoom;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    private Integer price;

    public SearchResultPojo() {
    }


}
